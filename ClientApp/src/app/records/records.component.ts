import { Component, OnInit } from '@angular/core';
import {Chart} from 'node_modules/chart.js';

@Component({
  selector: 'app-records',
  templateUrl: './records.component.html',
  styleUrls: ['./records.component.css']
})
export class RecordsComponent implements OnInit {
  public wins= 0;
  public lost = 0;
  constructor() { 
    if(localStorage.getItem('won'))
    {
      this.wins =  parseInt(localStorage.getItem('won'));
    }
    if(localStorage.getItem('lost'))
    {
      this.lost =  parseInt(localStorage.getItem('lost'));
    }
  }

  ngOnInit(): void {
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Won games", "Lost games"],
        datasets: [{
          labels: ["Won games" + this.wins, "Lost " +  this.lost],
            data: [this.wins, this.lost,],
            backgroundColor: [
                'rgba(155, 54, 54, 1)',
                'rgba(2, 48, 65, 1)'
            ],
            hoverBackgroundColor: 'black',
            borderColor: [
                'rgba(155, 54, 54, 1)',
                'rgba(2, 48, 65, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var data = {
  labels: ["Won Games " + this.wins, "Lost Games " +  this.lost],
  datasets: [{
    data: [this.wins, this.lost,],
    backgroundColor: [
        'rgba(155, 54, 54, 1)',
        'rgba(2, 48, 65, 1)'
    ],
    borderColor: [
        'rgba(155, 54, 54, 1)',
        'rgba(2, 48, 65, 1)'
    ],
    borderWidth: 1
}]
};
var ctx = document.getElementById('myChart2');
var myDoughnutChart = new Chart(ctx, {
  type: 'doughnut',
  data: data
});
  }
}

