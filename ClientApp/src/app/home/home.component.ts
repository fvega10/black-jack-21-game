import { Component } from '@angular/core';
import * as AOS from 'aos';
 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  public imgAssets: string = "/assets/fabricio_avatar_1.jpg";

  ngOnInit()
  {
    AOS.init();
  }
  mouseEnter(){
    this.imgAssets = "/assets/fabricio_avatar_2.jpg"
  }
  mouseLeave()
  {
    this.imgAssets = "/assets/fabricio_avatar_1.jpg"
  }
}
