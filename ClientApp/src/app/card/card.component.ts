import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {

  public cards: Card[];
  public card: Card;
  public isNull = false;

  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
    this.refresh();
  }
  
  refresh()
  {
    this.http.get<Card[]>(this.baseUrl + 'api/Cards').subscribe(result => {
      this.cards = result; 
      if(this.cards[0]['code'] != '')
      {
        this.isNull = true;    
      }
    }, error => console.error(error));
  }

  /**
   * Método permite crear las 52 cartas de una baraja de cartas
   * En este caso se emula el método de create en una aplicación
   */
  data()
  {
    //SPADES
    //HEART
    //CLOVER
    //DIAMOND
    this.cards = [
      {
        Value: 1,
        Image: "assets/AS.png",
        Suit: "SPADES",
        Code: "AS"
      },
      {
        Value: 1,
        Image: "assets/AH.png",
        Suit: "HEART",
        Code: "AH"
      },
      {
        Value: 1,
        Image: "assets/AC.png",
        Suit: "CLOVER",
        Code: "AC"
      },
      {
        Value: 1,
        Image: "assets/AD.png",
        Suit: "DIAMOND",
        Code: "AD"
      },

      {
        Value: 2,
        Image: "assets/2S.png",
        Suit: "SPADES",
        Code: "2S"
      },
      {
        Value: 2,
        Image: "assets/2H.png",
        Suit: "HEART",
        Code: "2H"
      },
      {
        Value: 2,
        Image: "assets/2C.png",
        Suit: "CLOVER",
        Code: "2C"
      },
      {
        Value: 2,
        Image: "assets/2D.png",
        Suit: "DIAMOND",
        Code: "2D"
      },

      {
        Value: 3,
        Image: "assets/3S.png",
        Suit: "SPADES",
        Code: "3S"
      },
      {
        Value: 3,
        Image: "assets/3H.png",
        Suit: "HEART",
        Code: "3H"
      },
      {
        Value: 3,
        Image: "assets/3C.png",
        Suit: "CLOVER",
        Code: "3C"
      },
      {
        Value: 3,
        Image: "assets/3D.png",
        Suit: "DIAMOND",
        Code: "3D"
      },

      {
        Value: 4,
        Image: "assets/4S.png",
        Suit: "SPADES",
        Code: "4S"
      },
      {
        Value: 4,
        Image: "assets/4H.png",
        Suit: "HEART",
        Code: "4H"
      },
      {
        Value: 4,
        Image: "assets/4C.png",
        Suit: "CLOVER",
        Code: "4C"
      },
      {
        Value: 4,
        Image: "assets/4D.png",
        Suit: "DIAMOND",
        Code: "4D"
      },

      {
        Value: 5,
        Image: "assets/5S.png",
        Suit: "SPADES",
        Code: "5S"
      },
      {
        Value: 5,
        Image: "assets/5H.png",
        Suit: "HEART",
        Code: "5H"
      },
      {
        Value: 5,
        Image: "assets/5C.png",
        Suit: "CLOVER",
        Code: "5C"
      },
      {
        Value: 5,
        Image: "assets/5D.png",
        Suit: "DIAMOND",
        Code: "5D"
      },

      {
        Value: 6,
        Image: "assets/6S.png",
        Suit: "SPADES",
        Code: "6S"
      },
      {
        Value: 6,
        Image: "assets/6H.png",
        Suit: "HEART",
        Code: "6H"
      },
      {
        Value: 6,
        Image: "assets/6C.png",
        Suit: "CLOVER",
        Code: "6C"
      },
      {
        Value: 6,
        Image: "assets/6D.png",
        Suit: "DIAMOND",
        Code: "6D"
      },

      {
        Value: 7,
        Image: "assets/7S.png",
        Suit: "SPADES",
        Code: "7S"
      },
      {
        Value: 7,
        Image: "assets/7H.png",
        Suit: "HEART",
        Code: "7H"
      },
      {
        Value: 7,
        Image: "assets/7C.png",
        Suit: "CLOVER",
        Code: "7C"
      },
      {
        Value: 7,
        Image: "assets/7D.png",
        Suit: "DIAMOND",
        Code: "7D"
      },

      {
        Value: 8,
        Image: "assets/8S.png",
        Suit: "SPADES",
        Code: "8S"
      },
      {
        Value: 8,
        Image: "assets/8H.png",
        Suit: "HEART",
        Code: "8H"
      },
      {
        Value: 8,
        Image: "assets/8C.png",
        Suit: "CLOVER",
        Code: "8C"
      },
      {
        Value: 8,
        Image: "assets/8D.png",
        Suit: "DIAMOND",
        Code: "8D"
      },

      {
        Value: 9,
        Image: "assets/9S.png",
        Suit: "SPADES",
        Code: "9S"
      },
      {
        Value: 9,
        Image: "assets/9H.png",
        Suit: "HEART",
        Code: "9H"
      },
      {
        Value: 9,
        Image: "assets/9C.png",
        Suit: "CLOVER",
        Code: "9C"
      },
      {
        Value: 9,
        Image: "assets/9D.png",
        Suit: "DIAMOND",
        Code: "9D"
      },

      {
        Value: 10,
        Image: "assets/10S.png",
        Suit: "SPADES",
        Code: "10S"
      },
      {
        Value: 10,
        Image: "assets/10H.png",
        Suit: "HEART",
        Code: "10H"
      },
      {
        Value: 10,
        Image: "assets/10C.png",
        Suit: "CLOVER",
        Code: "10C"
      },
      {
        Value: 10,
        Image: "assets/10D.png",
        Suit: "DIAMOND",
        Code: "10D"
      },

      {
        Value: 10,
        Image: "assets/JS.png",
        Suit: "SPADES",
        Code: "JS"
      },
      {
        Value: 10,
        Image: "assets/JH.png",
        Suit: "HEART",
        Code: "JH"
      },
      {
        Value: 10,
        Image: "assets/JC.png",
        Suit: "CLOVER",
        Code: "JC"
      },
      {
        Value: 10,
        Image: "assets/JD.png",
        Suit: "DIAMOND",
        Code: "JD"
      },

      {
        Value: 10,
        Image: "assets/QS.png",
        Suit: "SPADES",
        Code: "QS"
      },
      {
        Value: 10,
        Image: "assets/QH.png",
        Suit: "HEART",
        Code: "QH"
      },
      {
        Value: 10,
        Image: "assets/QC.png",
        Suit: "CLOVER",
        Code: "QC"
      },
      {
        Value: 10,
        Image: "assets/QD.png",
        Suit: "DIAMOND",
        Code: "QD"
      },

      {
        Value: 10,
        Image: "assets/KS.png",
        Suit: "SPADES",
        Code: "KS"
      },
      {
        Value: 10,
        Image: "assets/KH.png",
        Suit: "HEART",
        Code: "KH"
      },
      {
        Value: 10,
        Image: "assets/KC.png",
        Suit: "CLOVER",
        Code: "KC"
      },
      {
        Value: 10,
        Image: "assets/KD.png",
        Suit: "DIAMOND",
        Code: "KD"
      }    
    ];
    for (let i = 0; i < 53; i++) {
      this.card = this.cards[i];
      this.create();
    }
  }
  create(){
    this.http.post(this.baseUrl + 'api/Cards', this.card).subscribe(() => {
    }, error => console.error(error));
  }
}

interface Card {
  Value: number; //Valor
  Image: string; //Ubicación de la imágen
  Suit: string; //Tipo de carta:  corazones, rombos, tréboles y picas.
  Code: string; //Código de la carta
}
