import { Component, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CardComponent } from '../card/card.component';
import { CompileMetadataResolver } from '@angular/compiler';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent {
  public deck:           Deck;        //Baraja de cartas
  public cards:          Array<Card>; //Arreglo de cartas del usuario
  public cardsComputer:  Array<Card>; //Arreglo de cartas de la computadora
  public card:           Card;        //Objeto Carta del usuario
  public cardComputer:   Card;        //Objeto Carta de la computadora
  public draw:           Draw;        //Objeto Dibujar carta del usuario
  public drawComputer:   Draw;        //Objeto Dibujar carta de la computadora
  public isPlaying:      boolean;     //Permite conocer cuando el usuario desea iniciar a jugar
  public computerTurn:   boolean;     //Permite conocer si es el turno de la computadora
  public userPoints:     number;      //Permite conocer los puntos que lleva el usuario
  public computerPoints: number;      //Permite conocer los puntos que lleva la computadora
  public quantity:       number;      //Cantidad de cartas a solicitar
  public deck_id:        string;      //Identificador único de la baraja de cartas en juego
  public userWon         = 0;         //Cantidad de juegos ganados por el usuario
  public userLost      = 0;           //Cantidad de juegos perdidos por el usuario
  public hidden:         boolean;     //Si se muestra la carta oculta(volteada) o no
  constructor(private router:Router,public http: HttpClient, @Inject('BASE_URL') public baseUrl: string)
  {
    this.refresh();
  }
  /**
   * Método permite setear todas las variables públicas
   */
  private refresh()
  {
    this.hidden = true;
    this.loadWinsLost();                   //Carga las partidas jugadas, almacenadas en local storage
    this.deck           = new Deck();        //Baraja de cartas en juego
    this.card           = new Card();        //Objeto tipo Carta del usuario
    this.cardComputer   = new Card();        //Objeto tipo Carta de la computadora
    this.cards          = new Array<Card>(); //Arreglo de cartas del usuario
    this.cardsComputer  = new Array<Card>(); //Arreglo de cartas de la computadora
    this.draw           = new Draw();        //Objeto Dibujar carta del usuario
    this.drawComputer   = new Draw();        //Objeto dibujar carta de la computadora
    this.isPlaying      = false;             //Permite conocer si el usuario está jugando, y mostrar tags html en vista
    this.computerTurn   = false;             //Permite conocer el es el turno de la computadora y eliminar tags html en vista
    this.userPoints     = 0;                 //Puntos del usuario
    this.computerPoints = 0;                 //Puntos de la computadora
    this.quantity       = 1;                 //Cantidad de cartas a solicitar. Se utiliza en ambos: Usuario y computadora
    this.deck_id        = "";                //Identificación único de la baraja de cartas en juego
  }
  private semiRefresh()
  {
    this.hidden = true;
    this.loadWinsLost();
    this.card           = new Card();        //Objeto tipo Carta del usuario
    this.cardComputer   = new Card();        //Objeto tipo Carta de la computadora
    this.cards          = new Array<Card>(); //Arreglo de cartas del usuario
    this.cardsComputer  = new Array<Card>(); //Arreglo de cartas de la computadora
    this.draw           = new Draw();        //Objeto Dibujar carta del usuario
    this.drawComputer   = new Draw();        //Objeto dibujar carta de la computadora
    this.computerTurn   = false;             //Permite conocer el es el turno de la computadora y eliminar tags html en vista
    this.userPoints     = 0;                 //Puntos del usuario
    this.computerPoints = 0;                 //Puntos de la computadora
    this.quantity       = 1;                 //Cantidad de cartas a solicitar. Se utiliza en ambos: Usuario y computadora
  }
  /**
   * Método que permite cargas los datos de los juego almacenados
   * en local storage
   */
  private loadWinsLost()
  {
    if(localStorage.getItem('won'))
    {
      this.userWon =  parseInt(localStorage.getItem('won'));
    }
    if(localStorage.getItem('lost'))
    {
      this.userLost =  parseInt(localStorage.getItem('lost'));
    }
  }
  /**
   * Permite crear una Baraja de cartas => Deck
   */
  public createDeck()
  {
    this.http.post<Deck>(
      this.baseUrl 
      + 'api/Decks', 
      null).
    subscribe(result => 
    {
      this.isPlaying = true;              //¿Usuario en juego? Verdadero
      this.deck_id   = result['deck_id']; //Se setea el identificador único de la baraja de cartas en juego
      this.gameDefault(this.deck_id);     //Se llaman las primeras cartas por defecto para el usuario y para la computadora
    }, 
    error => console.error(error));
  }
  /**
   * 
   * @param deck_id Identificador único de la baraja en juego
   * Permite juego automático para el turno de la computadora
   */
  private gameDefault(deck_id:string)
  {
    this.http.post<Draw>(
      this.baseUrl 
      + 'api/Decks/' 
      + deck_id 
      + '/Draw/' 
      + 3, 3). //Se piden dos cartas para el usuario
    subscribe(result => 
    {
      this.drawComputer = result;     //Cartas para la computadora
      this.draw         = result;     //Cartas para el usuario
      this.setInitialCardsUser();     //Asigna 2 cartas al usuario
      //this.setInitialCardsComputer(); //Asigna 2 cartas a la computadora
    }, 
    error => console.error(error));
  }

  /**
   * Método permite setear las primeras 2 cartas al usuario
   */
  private setInitialCardsUser()
  {
    this.computerPoints = 0;
    this.userPoints = 0; //Se setea el valor en cero en caso de contener algún valor.
    this.quantity   = 1; //Se setea el valor en la vista del input: Give me
    //Ciclo recorre las otras 2 cartas y se las asigna al arreglo de cartas del usuario 
    for(let x:number = 0; x < this.draw.cards.length; x++)
    {
      if(x <= 1){
        this.card = this.draw.cards[x];
        this.cards.push(this.card);
      }else{
        this.cardComputer = this.draw.cards[x];
        //this.cardComputer.image = "/assets/computer_game.png"; //Se le asigna una ruta falsa, para que no muestre los valores
        this.cardsComputer.push(this.cardComputer);
      }
    }
    //Ciclo asigna puntaje al usuario según valor de cartas en el arreglo de cartas del usuario.
    for(let x:number = 0; x < this.cards.length; x++)
    {
      this.userPoints += this.cards[x].value;  
    }
    for(let x:number = 0; x < this.cardsComputer.length; x++)
    {
      this.computerPoints += this.cardsComputer[x].value;  
    }
  }
  /**
   * Método que permite solicitar N cantidad de cartas deseadas por el usuario
   * Se llama desde un botón en la vista
   */
  public WantCards()
  {
    var duplicateQuantity = this.quantity * 2;
    this.http.post<Draw>(
      this.baseUrl 
      + 'api/Decks/' 
      + this.deck_id 
      + '/Draw/' 
      + duplicateQuantity, 
      duplicateQuantity).
    subscribe(result => 
    {
      this.userPoints = 0; //Se setea el valor del puntaje del usuario en cero, ya que se volverá a hacer el conteo
      this.draw       = result;
      for(let x:number = 0; x < this.draw.cards.length; x++)
      {
        if(x <= this.quantity-1){
          this.card = this.draw.cards[x];
          this.cards.push(this.card); //Agrega las nuevas cartas al arreglo de cartas del usuario
        }else{
          this.cardComputer = this.draw.cards[x];
          this.cardsComputer.push(this.cardComputer);
        }
      }
      //se vuelve a setear el puntaje al usuario
      for(let x:number = 0; x < this.cards.length; x++){
        this.userPoints += this.cards[x].value;  
      }
      for(let x:number = 0; x < this.cardsComputer.length; x++)
      {
        this.computerPoints += this.cardsComputer[x].value;  
      }
      this.quantity   = 1; //Se setea el valor por defecto de la vista en el input de pedir cartas.
      //Consulta si el usuario ha perdido
      if(this.userPoints > 21)
      {
        this.hidden = false;
        localStorage.setItem('lost', (this.userLost + 1).toString()); //Almacena en local storage
        this.messageAlert(
          'Game Over!',
          "You are over 21: " + this.userPoints + " points",
          'error',
          "Go to home page",
          "Play again"
        );
      }
      else if (this.userPoints == 21) {
        this.computerGame();
      }
    },
    error => console.error(error));
  }

  /**
   * Método permite realizar el juego automático para la computadora
   */
  private computerGame()
  {
      this.hidden = false;
      this.http.post<Draw>(
        this.baseUrl 
        + 'api/Decks/' 
        + this.deck_id 
        + '/Draw/' 
        + 1, 1).
      subscribe(result => 
      {
        this.drawComputer = result;
        for(let x:number = 0; x < this.drawComputer.cards.length; x++)
        {
          this.cardComputer = this.drawComputer.cards[x];
          //this.cardComputer.image = "/assets/" + this.cardComputer.code + ".png"; //Se vuelve a setear la ruta real para volterar las 2 primeras cartas
          this.cardsComputer.push(this.cardComputer);
        }
        this.computerPoints = 0;
        for(let x:number = 0; x < this.cardsComputer.length; x++)
        {
          this.computerPoints += this.cardsComputer[x].value;  
        }
        this.resultCompare(); //Compara resultados
      }, 
      error => console.error(error));
  }

  shuffledCards(){
    // var cardss = this.allCards();
    this.cards  = new Array<Card>(); //Arreglo de cartas del usuario
    this.cardsComputer  = new Array<Card>();
    this.http.post<Deck>(
      this.baseUrl + 'api/Decks/'+ this.deck_id + '/shuffle', this.deck_id ).subscribe(result => 
    {
      this.deck = result;
      this.semiRefresh();
      this.messageAlertTwo(
        'Success!',
        "The cards were shuffled",
      );
      // this.gameDefault(this.deck.deck_id);
    }, 
    error => console.error(error));
    
  }
  // allCards(){
  //   for(let x:number = 0; x < this.cardsComputer.length; x++){
  //     this.cards.push(this.cardsComputer[x]);
  //   }
  //   return this.cards;
  // }
  /**
   * Método que permite comparar el resultado final.
   */
  private resultCompare(){
    this.computerTurn = true;
    if(this.userPoints == this.computerPoints)
    {
      this.messageAlert(
        'Tied game!',
        "User: " + this.userPoints + " /\n" + "Computer: " + this.computerPoints,
        'info',
        "Go to home page",
        "Play again"
      );
    }
    else if (this.computerPoints > 21) {
      localStorage.setItem('won', (this.userWon + 1).toString());
      this.messageAlert(
        'You win!',
        "User: " + this.userPoints + " /\n" + "Computer: " + this.computerPoints,
        'success',
        "Go to home page",
        "Play again"
      );
    }
    else if (this.userPoints > this.computerPoints) {
      localStorage.setItem('won', (this.userWon + 1).toString());
      this.messageAlert(
        'You win!',
        "User: " + this.userPoints + " /\n" + "Computer: " + this.computerPoints,
        'success',
        "Go to home page",
        "Play again"
      );
    }
    else
    {
      localStorage.setItem('lost', (this.userLost + 1).toString());
      this.messageAlert(
        'Game Over!',
        "User: " + this.userPoints + " /\n" + "Computer: " + this.computerPoints,
        'error',
        "Go to home page",
        "Play again"
      );
    }
  }
  /**
   * @param title Título de la alerta
   * @param text Mensaje principal que se desea mostrar
   * @param iconType Tipo de mensaje: Error, success, info, warning, danger
   * @param cancelBtn Texto del botón de cancelar
   * @param confirmBtn Texto del mensaje de confirmación 
   * Método permite mostrar alerta según resultado.
   */
  private messageAlert(title: string, text: string, iconType: any, cancelBtn: string, confirmBtn: string)
  {
    Swal.fire({
      title: title,
      text: text,
      width:310,
      imageWidth: 200,
      imageHeight: 200,
      backdrop: false,
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonColor: '#d33',
      cancelButtonText: cancelBtn,
      confirmButtonText: confirmBtn
    }).then((result) => 
    {
      if (result.value)
      {
        this.refresh();
        this.createDeck();
        
      }
      else
      {
        this.router.navigate(['/']);
      }
    });
  }
  private messageAlertTwo(title: string, text: string)
  {
    Swal.fire({
      title: title,
      text: text,
      width:310,
      onAfterClose: () => this.gameDefault(this.deck_id),
      timer: 1200,
      timerProgressBar: true,
      showConfirmButton: false,
      imageWidth: 100,
      imageHeight: 100,
      backdrop: false,
      allowOutsideClick: false,
    });
  }
}



class Deck {
  id: number;
  deck_id: string;
  card: Array<Card>;
  success: boolean;
  shuffled: boolean;
  remaining: number;
}
class Card {
  id: number;
  value: number; //Valor
  image: string; //Ubicación de la imágen
  suit: string; //Tipo de carta:  corazones, rombos, tréboles y picas.
  code: string; //Código de la carta
}
class Draw {
  id: number;
  success: boolean;
  cards: Array<Card>;
  deck_id: string;
  remaining: number;
}