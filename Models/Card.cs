namespace black_jack_21_game.Models
{
    public class Card
    {
        public long Id { get; set; }
        public string Image { get; set; } //Imágen de la carta
        public int Value { get; set; } // Valor de la cart: A, 2, k, etc
        public string Suit { get; set; } //Tipo de carta: De corazones, de trebol, etc
        public string Code { get; set; } //Código de la carta: KH -> K DE CORAZONES
    }
}
