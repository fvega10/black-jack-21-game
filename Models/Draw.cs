using System.Collections.Generic;

namespace black_jack_21_game.Models
{
    public class Draw
    {
        public long Id { get; set; }
        public bool Success { get; set; } // Identificador de la baraja de cartas
        public List<Card> Cards { get; set; } //Si esta barajada o no
        public string Deck_id { get; set; }
        public int Remaining { get; set; } //Cuantas cartas quedan
    }
}
