using System.Collections.Generic;

namespace black_jack_21_game.Models
{
    public class Deck
    {
        public int id { get; set; }
        public string Deck_id { get; set; }
        public bool Success { get; set; } // Identificador de la baraja de cartas
        public bool Shuffled { get; set; } //Si esta barajada o no
        public int Remaining { get; set; } //Cuantas cartas quedan
        public List<Card> Cards { get; set; }
    }
}
