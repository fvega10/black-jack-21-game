using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using black_jack_21_game.Models;
namespace black_jack_21_game.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DecksController : ControllerBase
    {
        private readonly DataBaseContext _context;
        public DecksController(DataBaseContext context)
        {
            _context = context;
        }
        /**
        * Método permite crear una Lista de cartas
        **/
        private List<Card> createCards(){
            List<Card> cards = new List<Card>();
            Card card        = new Card();
            int[] numbers    = new int[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10 };
            string[] letters = new string[]{ "CLUBS", "HEART", "DIAMOND", "SPADES" };
            string[] code    = new string[]{ "C", "H", "D", "S" };
            //Ciclo reccore los números de las cartas
            for(int i = 0; i < numbers.Length; i++)
            {
                //Ciclo recorre los tipo de valores
                for(int x = 0; x < code.Length; x++)
                {
                    card = new Card();
                    
                    if(i == 10){
                        card.Code =  "J" +  code[x];
                        card.Image = "/assets/J" + code[x] + ".png";
                    }else if(i == 11){
                        card.Code =  "Q" +  code[x];
                        card.Image = "/assets/Q" + code[x] + ".png";
                    }else if(i == 12){
                        card.Code =  "K" +  code[x];
                        card.Image = "/assets/K" + code[x] + ".png";
                    }else{
                        card.Code =  numbers[i] +  code[x];
                        card.Image = "/assets/" + numbers[i] + code[x] + ".png";
                    }
                    card.Suit =  letters[x];
                    card.Value = numbers[i];
                    cards.Add(card);
                }
            }
            cards = this.shuffledCards(cards); //Se llama al método que las desordena
            return cards;
        }
        /**
        * Método permite desordenar las cartas
        * Recibe por parámetro una lista de cartas
        **/
        private List<Card> shuffledCards(List<Card> cards)
        {
            List<Card> tmp      = new List<Card>(); 
            tmp                 = cards;
            List<Card> newCards = new List<Card>();
            Random randNum      = new Random();
            while (tmp.Count > 0)
            {
                int val = randNum.Next(0, tmp.Count - 1);
                newCards.Add(tmp[val]);
                tmp.RemoveAt(val);
            }
            return newCards;
        }
        /**
        * Método permite crear un Id de la baraja único
        **/
        private string createDeckId()
        {
            Random rdm = new Random();
            //Algorith to create deck_id personalized
            string[] key_letters = new string[] { "A", "B", "C", "D", "E", "f", "g", "h", "i", "j", "K", "L", "M", "N", "O", "P", "q", "r", "s", "T" };
            int[] key_number     = new int[] { 0,1,2,3,4,5,6,7,8,9 };
            var deck_id          = "dck.";
            for(int x = 0; x < 10; x++)
            {
                deck_id += key_letters[rdm.Next(0, key_letters.Length - 1)] + "" + key_number[rdm.Next(0, key_number.Length - 1)];
            }
            return deck_id;
        }
        /**
        * Método permite crear una baraja nueva
        **/
        private Deck createDeck()
        {
            List<Card> cards = new List<Card>();
            cards            = this.createCards();
            Deck deck        = new Deck();
            deck.id          = 0;
            deck.Deck_id     = this.createDeckId();
            deck.Cards       = cards;
            deck.Shuffled    = true;
            deck.Success     = true;
            deck.Remaining   = cards.Count;
            return deck;
        }
        private Deck createDeckWithId(string deck_id, int id)
        {
            List<Card> cards = new List<Card>();
            cards            = this.createCards();
            Deck deck        = new Deck();
            deck.id          = id;
            deck.Deck_id     = deck_id;
            deck.Cards       = cards;
            deck.Shuffled    = true;
            deck.Success     = true;
            deck.Remaining   = cards.Count;
            return deck;
        }
        // POST: api/Decks
        [HttpPost]
        public async Task<ActionResult<Deck>> PostDeck()
        {
            List<Card> cards = new List<Card>();
            cards            = this.createCards();
            Deck deck        = new Deck();
            deck.id          = 0;
            deck.Deck_id     = this.createDeckId();
            deck.Cards       = cards;
            deck.Shuffled    = true;
            deck.Success     = true;
            deck.Remaining   = cards.Count;
            _context.Decks.Add(deck);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtAction("GetDeck", deck);
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Deck>>> getDecks()
        {
            return await _context.Decks.Include(p => p.Cards).ToListAsync();
        }
        /**
        * Método permite buscar una baraja por número de id
        **/
        [HttpGet("{id}")]
        public async Task<ActionResult<Deck>> GetDeckById(int id)
        {
            var deck = await _context.Decks.Include(p => p.Cards).FirstOrDefaultAsync(i => i.id == id);
            if (deck == null)
            {
                return NotFound();
            }
            return deck;
        }
        /**
        * Método permite solicitar N cantidad de cartas en una baraja.
        * Las retorna y las elimina de la baraja
        **/
        // POST: api/Decks/<<deck_id>>/draw/count=# faltan validaciones y eliminar cartas en la baraja
        [HttpPost("{deck_id}/draw/{quantity}")]
        public async Task<IActionResult> PostDraw(string deck_id, int quantity)
        {
            List<Card>cartasTemp = new List<Card>(); 
            Draw draw            = new Draw();
            var cont             = 1;
            List<Card> cartas    = new List<Card>();
            var deck             = _context.Decks.Include("Cards").FirstOrDefault(e => e.Deck_id == deck_id);
            cartasTemp           = deck.Cards;
            deck.Remaining       = (deck.Remaining - quantity);
            draw.Deck_id         = deck_id;
            draw.Success         = deck.Success;
            draw.Remaining       = deck.Remaining;
            foreach(Card card in deck.Cards.ToList()){
                if(cont <= quantity){
                    cartas.Add(card);
                    cartasTemp.Remove(card);
                }else{
                    break;
                }
                cont++;
            }
            deck.Cards = cartasTemp;
            draw.Cards = cartas;
            _context.Draws.Add(draw);
            _context.Entry(deck).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return Ok(draw);
        }
        /**
        *   Método permite desordenar una baraja
        **/
         [HttpPost("{deck_id}/shuffle")]
        public async Task<IActionResult> shufle(string deck_id)
        {
            var deck = _context.Decks.Include("Cards").FirstOrDefault(e => e.Deck_id == deck_id);
            deck.Cards = this.createCards();
            deck.Cards = shuffledCards(deck.Cards);
            deck.Remaining = deck.Cards.Count;
            _context.Entry(deck).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
           return Ok(deck);
        }
        /**
        * Método permite confirmar si existe una baraja de cartas lista para usar
        **/
        private bool DeckExists(string id)
        {
            return _context.Decks.Any(e => e.Deck_id == id);
        }
    }
}
