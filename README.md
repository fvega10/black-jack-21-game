Correr los siguiente comandos:

```
dotnet new angular
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.InMemory
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet tool install --global dotnet-aspnet-codegenerator
```
## Migración Angular más reciente

Correr los siguientes comandos, dentro de la carpeta **/ClientApp**:

```
npm install --save -dev @angular/cli@latest
npm install -g @angular/cli@latest
ng update
ng update @angular/cli @angular/core
ng update @angular/cli @angular/core --allow-dirty --force
ng build
```

Para crear un nuevo componente, se ejecuta el siguiente comando:

```
ng g c new-component --module app
```

## Models

Se creó una carpeta llamada Models y se le agregaron los diferentes modelos

> DataBaseContext

> Card

> Deck

## Controller

Se crea la carpeta llamada Controller

## Startup

Se debe indicar cual será nuestra base de datos en el archivo **Startup.cs**

Y se agrega la siguiente línea de código, dentro del método llamadao **ConfigureServices**:

```
 services.AddDbContext<DataBaseContext>(opt => opt.UseInMemoryDatabase("Cards"));
```

## SweetAlert2

Se debe de instalar una librería para la visualización de las alertas.

Para ello se debe de ingresar en la carpeta */ClientApp/* y digitar el siguiente comando:

```
npm install sweetalert2
```

## AOS Library

Permite tener animaciones

```
npm install aos --save
```

